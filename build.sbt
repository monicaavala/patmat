name := "patmat"

scalaVersion := "2.12.1"

scalacOptions ++= Seq(
  "-deprecation",
  "-unchecked"
)

fork := true

javaOptions += "-Xmx2G"

parallelExecution in Test := false

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"

libraryDependencies += "junit" % "junit" % "4.12" % "test"
